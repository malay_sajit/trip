/**
 * Created by malayk on 8/19/14.
 */
(function () {
    var app = angular.module('testApp', ['google-maps', 'ngRoute', 'ui.bootstrap']);

    CITY = null;

    app.controller('backgroundController', function ($scope, $element) {
        var body = $element[0];

        function getRandomInt(min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }

        var imageUrl = "/public/images/background/" + parseInt(getRandomInt(0, 7)) + ".jpg";

        body.style.backgroundImage = "url(" + imageUrl + ")";
    });

    app.controller('TypeaheadCtrl', function ($scope, $element, $window) {

        var body = $element[0];

        function getRandomInt(min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }

        var imageUrl = "/public/images/background/" + parseInt(getRandomInt(0, 7)) + ".jpg";

        $scope.init = function(){
            body.style.backgroundImage = "url(" + imageUrl + ")";
            var myImage = new Image();
            body.appendChild(myImage);
            myImage.style.opacity = "0.0";
            myImage.style.height = "0px";
            myImage.style.width = "0px";
            myImage.onload = function(){
                body.style.backgroundImage = "url(" + myImage.src + ")";
                $scope.$apply();
            };
            $scope.$watch(function () {
                return $scope.selected;
            }, function (value) {
                if($scope.states.indexOf($scope.selected) != -1){
                    myImage.src = '/public/images/city/'+$scope.selected.toLowerCase()+'.jpg';
                }
            });
        };



        $scope.selected = undefined;
        $scope.states = ['Bangkok', 'London', 'Milan', 'Paris', 'Rome', 'Singapore', 'Shanghai', 'New York', 'Amsterdam', 'Istanbul', 'Tokyo', 'Dubai', 'Vienna', 'Kuala Lumpur', 'Taipei', 'Hong Kong', 'Riyadh', 'Barcelona', 'Los Angeles'];

        $scope.day = 1;
        $scope.dayString = "1 day trip"
        $scope.setDay = function (day) {
            $scope.day = day;
            if (day == 1) {
                $scope.dayString = day + " day trip";
            } else {
                $scope.dayString = day + " days trip";
            }
        };
        $scope.enterCityPage = function () {
            if ($scope.selected == undefined) {
                return;
            }
            var inputField = document.getElementById('tags');
            if($scope.states.indexOf($scope.selected) == -1){
                inputField.style.borderColor = "rgba(239, 39, 0, 0.80)";
                inputField.style.boxShadow = "0 1px 1px rgba(0, 0, 0, 0.075) inset, 0 0 8px rgba(239, 39, 0, 0.60)";
                inputField.style.outline = "0 none";
                return;
            }
            inputField.style.borderColor = "";
            inputField.style.boxShadow = "";
            inputField.style.outline = "";
            $window.open('/entercity?city=' + $scope.selected + "&days=" + $scope.day + "&category="+ "1111111", '_self');
        }

    });

    app.controller('mapCtrl', function ($scope, $timeout, $rootScope) {
        $scope.map = {center: {latitude: 51.219053, longitude: 4.404418 }, zoom: 1 };
        $scope.options = {scrollwheel: false};
        $scope.markers = [];
        $scope.templateOption = {
            pixelOffset: {width: 0, height: -27}
        };
        var style = document.createElement('style');
        style.innerHTML = '.labelMarker ' +
            '{ ' +
            'font-size: 12px; ' +
            'font-weight: bold; ' +
            'color: #FFFFFF;' +
            'font-family: "DINNextRoundedLTProMediumRegular";' +
            'border-radius: 50%; ' +
            'width: 17px;text-align: center;' +
            'vertical-align: middle;' +
            'line-height: 20px; ' +
            '}';
        style.type = 'text/css';
        document.getElementsByTagName('head')[0].appendChild(style);
        $scope.$on("cityDataReceived", function (event, args) {
            var avgLat = 0.0;
            var avgLon = 0.0;
            var countOfAttractions = 0;
            $scope.markers = [];
            var City = args.City;
            var dayNum = 0;
            var dayImage = "/public/images/Day";

            for (var day in City) {
                var attrNum = 0;
                var destinationList = City[day].destinationList;

                dayNum += 1;
                for (var attraction in destinationList) {
                    attraction = destinationList[attraction];
                    countOfAttractions += 1;

                    avgLat += parseFloat(attraction.latitiude);
                    avgLon += parseFloat(attraction.longitude);
                    $scope.markers.push({
                        id: attraction.id,
                        title: attraction.name,
                        show: false,
                        templateParam: {
                            title: attraction.name,
                            imageUrl:attraction.imageUrl,
                            category:attraction.category
                        },
                        templateURL: '/public/angular/templates/infowindow.html',
                        coords: {
                            latitude: attraction.latitiude,
                            longitude: attraction.longitude
                        },
                        options: {
                            animation: null,
                            draggable: false,

                            icon: dayImage + dayNum.toString() + ".png",

                            title: attraction.name
                        },
                        dayId: dayNum - 1,
                        attrId: attrNum,
                        isMouseOver: false
                    });
                    attrNum += 1;
                }
            }
            avgLat /= parseFloat(countOfAttractions);
            avgLon /= parseFloat(countOfAttractions);
            $scope.map = {
                center: {
                    latitude: avgLat,
                    longitude: avgLon
                },
                zoom: 14,
                fit: true,
                refresh: true
            };
            $scope.options = {
                scrollwheel: false,
                refresh: true
            };

            $scope.markersEvents = {
                click: function (gMarker, eventName, model) {
                    if (model.$id) {
                        model = model.coords;//use scope portion then
                    }
                    if (model.isMouseOver == true) {
                        return;
                    }
//                    model.isMouseOver = true;
                    model.show = true;
//                    UNCOMMENT
                    $scope.$digest();
                }
            };
            $rootScope.showLoading = false;
        });

        $scope.$on("mouseOverDay", function (event, args) {
            dayId = args.dayId;
            for (var i = 0; i < $scope.markers.length; i++) {
                if ($scope.markers[i].dayId == dayId) {

                    $scope.markers[i].options.animation = google.maps.Animation.BOUNCE;
                }
                else {
                    $scope.markers[i].options.animation = null;

                }
                $scope.markers[i].show = false;
            }
//            UNCOMMENT
            $scope.$digest();
        });

        $scope.$on("mouseOverAttraction", function (event, args) {
            dayId = args.dayId;
            attrId = args.attrId;

            for (var i = 0; i < $scope.markers.length; i++) {
                if ($scope.markers[i].dayId == dayId && $scope.markers[i].attrId == attrId) {
                    $scope.markers[i].show = true;
                }
                else {
                    $scope.markers[i].show = false;
                }
            }
            $scope.$digest();
        });

        $scope.$on("removeAllAnimations", function (event, args) {
            for (var i = 0; i < $scope.markers.length; i++) {
                $scope.markers[i].options.animation = null;
                $scope.markers[i].show = false;
            }
            $scope.$digest();
        })

    });

    app.directive('itineraryResult', function () {
        return {
            restrict: 'E',
            templateUrl: '/public/angular/templates/itinerary-result.html'
        };
    });

    app.directive('cityForm', function () {
        return {
            restrict: 'E',
            templateUrl: '/public/angular/templates/city-form.html'
        };
    });

    app.controller('cityFormController', function ($scope, $http, $location, $element, $rootScope, $timeout, $window) {
        $scope.cityName = null;
        $scope.days = null;
        $rootScope.showLoading = true;
        $scope.data = null;
        $scope.showPlanner = true;
        $scope.showMap = false;
        $rootScope.CITY = $rootScope.data;
        $rootScope.CITYCHANGED = false;
        $scope.showModal = false;
        $scope.showSavingButton = false;
        $scope.categories = ["Outdoor Activities","Historical/Religious", "Landmarks","Education/Art","Natural","Leisure", "Others" ];
        $scope.categoryIsSelected = [true, true, true, true, true, true, true];
        $scope.showCategoryMenu = false;
        $scope.showApplyButton  = false;

        $scope.getImageForIndex = function(idx)
        {
            if($scope.categoryIsSelected[idx]) {
                return  'url(/public/images/tick.png)';
            }
            else {
                return "none";
            }
        };

        $scope.$watch(function (){
            return $scope.categoryIsSelected;
        }, function (value) {
            $scope.showApplyButton = true;

        }, true);

        $scope.toggleCategoryMenu = function () {
            $scope.showCategoryMenu = !$scope.showCategoryMenu;
             arrow = document.getElementById('arrowElement');
            if(arrow.style.webkitTransform == ""){
                arrow.style.webkitTransform = 'rotate(180deg)';
            }
            else {
                arrow.style.webkitTransform = "";
            }
        };

        $scope.selectCategory = function(idx){
            $scope.categoryIsSelected[idx] = !$scope.categoryIsSelected[idx];
        };

        $scope.setMap = function () {
            $rootScope.showLoading = true;
            $scope.showPlanner = false;
            $scope.showMap = true;
            $scope.isCollapsed = false;

            $timeout(function () {
                $scope.$broadcast("cityDataReceived", {City: $rootScope.CITY });
            }, 10, true);

        };

        $scope.saveTrip = function(){
            $scope.showSavingButton = true;

            $http.post('/getItineraryId.JSON', $rootScope.CITY).
                success(function (data, status, headers, config) {
                    $window.open('/trip?id=' + data, '_self');
                }).
                error(function (data, status, headers, config) {
                    alert("Could Not Save Trip. Try Again Later.");
                });
        };

        $scope.setPlanner = function () {
            $timeout(function () {
                $rootScope.showLoading = false;
                $scope.showPlanner = true;
                $scope.showMap = false;
                $scope.isCollapsed = true;
            });

        };

        $scope.getCityDetails = function (cityName, nod) {
            var states = ['Bangkok', 'London', 'Milan', 'Paris', 'Rome', 'Singapore', 'Shanghai', 'New York', 'Amsterdam', 'Istanbul', 'Tokyo', 'Dubai', 'Vienna', 'Kuala Lumpur', 'Taipei', 'Hong Kong', 'Riyadh', 'Barcelona', 'Los Angeles'];

            if(states.indexOf(cityName) == -1){
                var inputField = document.getElementById('cityInputField');
                inputField.style.borderColor = "rgba(239, 39, 0, 0.80)";
                inputField.style.boxShadow = "0 1px 1px rgba(0, 0, 0, 0.075) inset, 0 0 8px rgba(239, 39, 0, 0.60)";
                inputField.style.outline = "0 none";
                return;
            }
            if(document.getElementById('cityInputField') != null) {
                var inputField = document.getElementById('cityInputField');
                inputField.style.borderColor = "";
                inputField.style.boxShadow = "";
                inputField.style.outline = "";
            }

            if(nod > 5){
                var inputField = document.getElementById('dayInputField');
                inputField.style.borderColor = "rgba(239, 39, 0, 0.80)";
                inputField.style.boxShadow = "0 1px 1px rgba(0, 0, 0, 0.075) inset, 0 0 8px rgba(239, 39, 0, 0.60)";
                inputField.style.outline = "0 none";
                return;
            }
            if(document.getElementById('dayInputField') != null) {
                var inputField = document.getElementById('dayInputField');
                inputField.style.borderColor = "";
                inputField.style.boxShadow = "";
                inputField.style.outline = "";
            }

            $scope.data = null;
            $rootScope.CITY = null;

            if($scope.showCategoryMenu)
            {
                $scope.toggleCategoryMenu();
            }
            $scope.showApplyButton  = false;
            var bitMap = "";
            for(var i = 0; i < $scope.categories.length; i++){
                if($scope.categoryIsSelected[i]){
                    bitMap += '1';
                }
                else {
                    bitMap += '0';
                }
            }
            $http.get('/city.JSON', {
                params: {
                    city: cityName,
                    days: nod,
                    category:bitMap
                }
            }).
                success(function (data, status, headers, config) {
                    // TO DO. REMOVE THIS. AND GET THIS FROM THE JSON ITSELF //
                    $rootScope.currentCityName = cityName;
                    data.cityName = $scope.cityName;
                    $scope.showApplyButton  = false;
                    $scope.data = data;
                    $rootScope.CITY = data;
                    $rootScope.CITYCHANGED = true;
                    $scope.setMap();

                }).
                error(function (data, status, headers, config) {
                    alert("Could Not Connect To Server");
                });
        };

        function loadFromUrl() {
            var url = $location.absUrl();
            url = decodeURIComponent(url);
            url = url.substring(url.indexOf('?') + 1);
            var params = url.split('&');
            var Params = {};
            for (param in params) {
                var x = params[param].split('=');
                x[1]= x[1].split('#')[0];
                Params[x[0]] = x[1];
            }
            $scope.cityName = Params.city;
            $scope.days = Params.days;
            $scope.getCityDetails(Params.city, Params.days);
        }

        loadFromUrl();

        var leftPanel = $element[0].children[1].children[0].children[0];
        var curOnlyDayId = null;
        var curDayId = null;
        var curAttrId = null;
        leftPanel.onmouseover = function (event) {
            var target = event.srcElement;
            var idx = target.id.split('_');
            if (idx[0] == "HEADER") {
                if (curOnlyDayId != idx[1]) {
                    curOnlyDayId = idx[1];

                    $scope.$broadcast("mouseOverDay", {dayId: idx[1]});
                }

            }
            else if (idx[0] == "ATTR") {
                if (curDayId != idx[1] || curAttrId != idx[2]) {
                    curDayId = idx[1];
                    curAttrId = idx[2];

                    $scope.$broadcast("mouseOverAttraction", {dayId: idx[1], attrId: idx[2]});

                }
            }
        };

        leftPanel.onmouseout = function (event) {
            curAttrId = null;
            curDayId = null;
            curOnlyDayId = null;

            $scope.$broadcast("removeAllAnimations", {});
        };

    });

    app.directive('plannerView', function () {
        return {
            restrict: 'E',
            templateUrl: '/public/angular/templates/planner.html'
        };
    });

    app.controller('plannerController', function ($scope, $element, $window, $rootScope, $http) {



        var topBarHeight = 57;

        var TOTAL_HEIGHT = 1440.0;
        var TOTAL_TIME = 24.0 * 60 * 60;

        var leftPanel = $element[0].parentElement.parentElement.parentElement.parentElement.parentElement.children[0];
        var fatherOfTimeline = $element[0].children[0].children[0].children[1].children[0].children[0];
        var fatherOfHeader = $element[0].children[0].children[0].children[1].children[0].children[1].children[0];
        var dayHeader = fatherOfHeader.children[1];
        var dayHeaderOffset = parseInt(dayHeader.style.left);
        var timeline = $element[0].children[0].children[0].children[1].children[0].children[0].children[0];
        var dayBoardElement = $element[0].children[0].children[0].children[1].children[0];


        fatherOfTimeline.style.left = parseInt(leftPanel.getBoundingClientRect().width + 30) + "px";
        fatherOfTimeline.style.height = ($window.innerHeight - topBarHeight - 75 - 13) + 'px';

        var LEFT_OFFSET = leftPanel.getBoundingClientRect().right;
        var LEFT_OFFSET_FOR_HEADER = LEFT_OFFSET + 80;

        fatherOfHeader.style.left = parseInt(LEFT_OFFSET_FOR_HEADER) + 'px';

        $scope.addButtons = null;
        $scope.placeName = undefined;
        if($rootScope.places == undefined) {
            $rootScope.places = ['One Piece', 'Alchemy', 'Totoro' ];
        }

        if ($rootScope.itinerary == undefined) {
            $rootScope.itinerary = null;
        }
        else {
            updateAddAttrButtonPos();
        }

        $scope.timeString = [
            "00 AM",
            "01 AM",
            "02 AM",
            "03 AM",
            "04 AM",
            "05 AM",
            "06 AM",
            "07 AM",
            "08 AM",
            "09 AM",
            "10 AM",
            "11 AM",
            "00 PM",
            "01 PM",
            "02 PM",
            "03 PM",
            "04 PM",
            "05 PM",
            "06 PM",
            "07 PM",
            "08 PM",
            "09 PM",
            "10 PM",
            "11 PM"
        ];

        function getCopyOfArray(array) {
            var ret = [];
            for (var i = 0; i < array.length; i++) {
                ii = [];
                for (var j = 0; j < array[i].length; j++) {
                    ii.push({
                        name: array[i][j].name,
                        type: array[i][j].type,
                        image: array[i][j].image,
                        top: array[i][j].top,
                        height: array[i][j].height
                    })
                }
                ret.push(ii);
            }
            return ret;
        }

        $scope.backUpItinerary = null;

        $scope.insertDay = function (afterDayId) {
            if ($rootScope.itinerary.length >= 5) {
                alert("Cannot Add More than 5 Days");
                return;
            }
            $rootScope.itinerary.splice(afterDayId + 1, 0, []);
            dayBoardPos = initDayBoardPos(dayBoardElement);
//            UNCOMMENT ATTR
//            updateAddAttrButtonPos();
        };

        $scope.removeDay = function (dayId) {
            if ($rootScope.itinerary.length <= 1) {
                alert("You cannot Remove the Last Day ");
                return;
            }
            $rootScope.itinerary.splice(dayId, 1);
            dayBoardPos = initDayBoardPos(dayBoardElement);
//            UNCOMMENT ATTR
//            updateAddAttrButtonPos();
        };

        dayBoardElement.style.height = parseInt($window.innerHeight - topBarHeight - 75) + 'px';

        function initDayBoardPos(element) {
            var ret = [];
            var start = 90 + LEFT_OFFSET;
            var diff = 354;
            for (var i = 0; i < $rootScope.itinerary.length; i++) {
                ret.push({left: start});
                start += diff;
            }
            ;
            return ret;
        };

        var dayBoardPos = null;

        function getCurrentDayBoard(pos) {
            pos.left -= $scope.LEFT_SCROLL_OFFSET;
            var effectiveLeft =pos.left - $scope.LEFT_SCROLL_OFFSET - 30;
            var cntr = (effectiveLeft) + (pos.width) / 2;
            for (var i = 0; i < dayBoardPos.length - 1; i++) {
                if (cntr >= dayBoardPos[i].left && cntr <= dayBoardPos[i + 1].left) {
                    return i;
                }
            }
            return dayBoardPos.length - 1;
        }

        function updateAddAttrButtonPos() {
            if (dayBoardPos == null)
                dayBoardPos = initDayBoardPos(dayBoardElement);
            var days = $rootScope.itinerary.length;


            $scope.addButtons = [];
            var ADJUST_TOP = 10;
            var ADJUST_LEFT = 16 + LEFT_OFFSET;
            var TOTAL_HEIGHT = 1440;
            var addPlaceMinHeightReqd = 100;
            for (var day = 0; day < days; day++) {

                $scope.addButtons.push([]);
                var attrCount = $rootScope.itinerary[day].length;
                var topOfAtr = 0;
                if (attrCount == 0) {

                    topOfAtr = TOTAL_HEIGHT;
                    $scope.addButtons[day].push({
                        top: (Math.floor(topOfAtr / 2) - ADJUST_TOP),
                        left: dayBoardPos[day].left - ADJUST_LEFT,
                        dayId:day,
                        attrId:0
                    });
                    continue;
                }
                var prevPos = 0;
                for (var attr = 0; attr < attrCount; attr++) {

                    topOfAtr = $rootScope.itinerary[day][attr].top;

                    if (topOfAtr - prevPos > addPlaceMinHeightReqd) {

                        $scope.addButtons[day].push({
                            top: (prevPos + Math.floor((topOfAtr - prevPos) / 2) - ADJUST_TOP),
                            left: dayBoardPos[day].left - ADJUST_LEFT,
                            dayId:day,
                            attrId:attr
                        });
                    }
                    prevPos = topOfAtr + $rootScope.itinerary[day][attr].height;

                }
                topOfAtr = TOTAL_HEIGHT;
                if (topOfAtr - prevPos > addPlaceMinHeightReqd) {
                    $scope.addButtons[day].push({
                        top: (prevPos + Math.floor((topOfAtr - prevPos) / 2) - ADJUST_TOP),
                        left: dayBoardPos[day].left - ADJUST_LEFT,
                        dayId:day,
                        attrId:$rootScope.itinerary[day].length
                    });
                }
            }

            //UNCOMMENT
            if (!$scope.$$phase && !$scope.$root.$$phase) {
                $scope.$digest();
            }
        }

        dayBoardElement.onscroll = function (event) {
            timeline.style.top = -dayBoardElement.scrollTop + "px";
            var prev = dayHeaderOffset - dayBoardElement.scrollLeft;
            dayHeader.style.left = parseInt(prev) + "px";
            $scope.LEFT_SCROLL_OFFSET = prev;
        };

        function newIndex(dayId, top, height, idx) {
            if(top < 0){
                return -1;
            }
            var ret = 0;
            for (var i = 0; i < $rootScope.itinerary[dayId].length; i++) {
                var ttop = $rootScope.itinerary[dayId][i].top;
                var theight = $rootScope.itinerary[dayId][i].height;
                if (ttop < top)
                    ret = i + 1;
                if (ttop < top && ttop + theight >= top) {
                    return -1;
                }
                if (top < ttop && top + height > ttop) {
                    return -1;
                }
            }
            return ret;
        }

        function updateUndo() {
            $scope.backUpItinerary = getCopyOfArray($rootScope.itinerary);
            $scope.$digest();
        }

        $scope.undo = function () {
            if ($scope.backUpItinerary == null)
                return;
            $scope.addButtons = [];
            $rootScope.itinerary = $scope.backUpItinerary;
            $scope.backUpItinerary = null;

            updateAddAttrButtonPos();
        }

        var handleMouseMove = false;

        dayBoardElement.onmousedown = function (event) {

            if (dayBoardPos == null)
                dayBoardPos = initDayBoardPos(dayBoardElement);

            var clickedElement = event.srcElement;


            if (clickedElement.id == "resizeHandle") {
                $scope.addButtons = [];
                updateUndo();

                parent = parent = clickedElement.parentElement;
                dayId = parent.parentElement.parentElement.parentElement.id;
                attractionId = parent.parentElement.id;

                handleMouseMove = true;
                mouseClicked = {
                    'type': 'resizeElement',
                    'element': clickedElement,
                    'oldheight': $rootScope.itinerary[dayId][attractionId].height,
                    'attractionId': attractionId,
                    'dayId': dayId,
                    'parent': parent,
                    'pos': {
                        'x': event.x,
                        'y': event.y
                    },

                    'handler': function (clickInfo, event) {
                        var newHeight = clickInfo.oldheight + (event.y - clickInfo.pos.y);
                        var nAttr = parseInt(clickInfo.attractionId) + 1;
                        var upLimit;
                        if (nAttr >= $rootScope.itinerary[clickInfo.dayId].length)
                            upLimit = 5000;
                        else
                            upLimit = $rootScope.itinerary[clickInfo.dayId][nAttr].top - $rootScope.itinerary[clickInfo.dayId][clickInfo.attractionId].top;
                        if (newHeight > 20 && newHeight <= upLimit) {
                            $rootScope.itinerary[clickInfo.dayId][clickInfo.attractionId].height = newHeight;
                            clickInfo.parent.style.height = parseInt(newHeight) + "px";
                            clickInfo.parent.children[0].style.height = parseInt(newHeight - 10) + "px";
                        }
                    },
                    'done': function (event) {

                        updateAddAttrButtonPos();
                    }
                };
            }
            else if (clickedElement.id == "cardDraggable" || clickedElement.id == "cardDraggable_child") {

                $scope.addButtons = [];
                updateUndo();

                parent = clickedElement.parentElement.parentElement;
                if (clickedElement.id == "cardDraggable_child")
                    parent = parent.parentElement;
                var dayId = parent.parentElement.parentElement.parentElement.id;
                var attractionId = parent.parentElement.id;


                handleMouseMove = true;
                parent.style.zIndex = 9999;
                parent.style.boxShadow = "2px 2px 1.2em gray";
                mouseClicked = {
                    'type': 'cardDragNDrop',
                    'element': clickedElement,
                    'oldheight': $rootScope.itinerary[dayId][attractionId].height,
                    'oldtop': $rootScope.itinerary[dayId][attractionId].top,
                    'attractionId': attractionId,
                    'dayId': dayId,
                    'parent': parent,
                    'currentColumn': dayId,
                    'pos': {
                        'x': event.x,
                        'y': event.y
                    },

                    'handler': function (info, event) {

                        var dx = event.x - info.pos.x;
                        var dy = event.y - info.pos.y;

                        var x = dx;
                        var y = info.oldtop + dy;

                        info.parent.style.top = parseInt(y) + "px";
                        info.parent.style.left = parseInt(x) + "px";

                        var element = info.parent;
                        var position = element.getBoundingClientRect();
                        info.currentColumn = getCurrentDayBoard(position);


                        $rootScope.itinerary[info.dayId][info.attractionId].top = (info.oldtop + dy);
                    },
                    'done': function (info) {
                        info.parent.zIndex = "auto";
                        info.parent.style.boxShadow = "";

                        var attraction = $rootScope.itinerary[info.dayId][info.attractionId];

                        var idx = newIndex(info.currentColumn, attraction.top, attraction.height, {i: info.dayId, j: info.attractionId});

                        if (idx == -1) {
                            $scope.$apply(function () {
                                $rootScope.itinerary[info.dayId][info.attractionId].top = info.oldtop;
                                $rootScope.itinerary[info.dayId][info.attractionId].height = info.oldheight;
                            });
                            info.parent.style.top = parseInt(info.oldtop) + "px";
                            info.parent.style.left = 0 + "px";
                        }
                        else {
                            $scope.$apply(function () {
                                var deletedElement = $rootScope.itinerary[info.dayId].splice(info.attractionId, 1);
                                $rootScope.itinerary[info.currentColumn].splice(idx, 0, deletedElement[0]);
                            });
                        }
                        updateAddAttrButtonPos();
                    }
                };


            }
            else if(clickedElement.id == "deleteButton"){
                var attrId = clickedElement.parentElement.parentElement.parentElement.id;
                var dayId = clickedElement.parentElement.parentElement.parentElement.parentElement.parentElement.id;

                $scope.$apply(function(){
                    $rootScope.itinerary[dayId].splice(attrId,1);
                });

            }
            else if (clickedElement.className.indexOf("day dropArea") != -1 ) {

                $scope.addButtons = [];
                updateUndo();

                parent = clickedElement;
                dayId = parent.id


                handleMouseMove = true;
                parent.style.zIndex = 1;
                parent.style.boxShadow = "2px 2px 1.2em gray";
                mouseClicked = {
                    'type': 'dayDragNDrop',
                    'element': clickedElement,
                    'oldleft': parent.style.left,
                    'dayId': dayId,
                    'parent': parent,
                    'currentColumn': dayId,
                    'pos': {
                        'x': event.x,
                        'y': event.y
                    },

                    'handler': function (info, event) {

                        var dx = event.x - info.pos.x;


                        var x = dx;

                        info.parent.style.left = parseInt(x) + "px";

                        var element = info.parent;
                        var position = element.getBoundingClientRect();
                        info.currentColumn = getCurrentDayBoard(position);
                    },
                    'done': function (info) {
                        info.parent.style.zIndex = "auto";
                        info.parent.style.boxShadow = "";
                        info.parent.style.left = "";
                        if (info.currentColumn != info.dayId) {
                            var day = $rootScope.itinerary.splice(info.dayId,1)[0];
                            $scope.$apply(function () {
                                $rootScope.itinerary.splice(info.currentColumn,0, day);

                                updateAddAttrButtonPos();
                            });
                        }
                        else {
                            info.parent.style.zIndex = "auto";
                            info.parent.style.boxShadow = "";
                            info.parent.style.left = "";

                            updateAddAttrButtonPos();
                        }

                    }
                };


            }
        };

        dayBoardElement.onmousemove = function (event) {
            if (handleMouseMove == true)
                mouseClicked.handler(mouseClicked, event);
        }

        dayBoardElement.onmouseup = function (event) {
            if (handleMouseMove == true) {
                handleMouseMove = false;
                mouseClicked.done(mouseClicked);
                mouseClicked = null;
            }
        }

        function getTopFor(seconds) {
            return (seconds / TOTAL_TIME) * TOTAL_HEIGHT;
        }

        function getHeightFor(duration) {
            return getTopFor(duration);
        }

        function getStartTimeFor(top) {
            return parseInt((top / TOTAL_HEIGHT) * TOTAL_TIME);
        }

        function getDurationFor(height) {
            return getStartTimeFor(height);
        }

        function getItineraryAttractionFromCityAttraction(attr){
            return {
                id: attr.id,
                name: attr.name,
                type: attr.category,
                top: getTopFor(attr.startTime),
                height: getHeightFor(attr.duration),
                image: attr.imageUrl,
                latitiude: attr.latitiude,
                longitude: attr.longitude
            };
        }

        function updateItinerary() {
            var tempIt = [];
            var days = $rootScope.CITY.length;
            for (var i = 0; i < days; i++) {
                tempIt.push([]);
                var attrs = $rootScope.CITY[i].destinationList.length;
                for (var j = 0; j < attrs; j++) {
                    var attr = $rootScope.CITY[i].destinationList[j];
                    tempIt[i].push(getItineraryAttractionFromCityAttraction(attr));
                }
            }
            $rootScope.itinerary = tempIt;
            $rootScope.places = ['OnePiece'];

            dayBoardPos = initDayBoardPos(dayBoardElement);

            updateAddAttrButtonPos();
        }

        function updateGlobalCity() {
            var days = $rootScope.itinerary.length;
            var tempCity = [];
            for (var i = 0; i < days; i++) {
                var destList = [];
                var attrs = $rootScope.itinerary[i].length;
                for (var j = 0; j < attrs; j++) {
                    var attr = $rootScope.itinerary[i][j];
                    destList.push({
                        id: attr.id,
                        name: attr.name,
                        category: attr.type,
                        startTime: getStartTimeFor(attr.top),
                        duration: getDurationFor(attr.height),
                        imageUrl: attr.image,
                        latitiude: attr.latitiude,
                        longitude: attr.longitude
                    });
                }
                tempCity.push({
                    day: "Day " + parseInt(i + 1),
                    destinationList: destList
                });
            }
            $rootScope.CITY = tempCity;


        }

        function updateTypeAheadList(){

            $http.get('/attractions.JSON', {
                params: {
                    city: $rootScope.currentCityName
                }
            }).
                success(function (data, status, headers, config) {
                    $rootScope.places = data;

                }).
                error(function (data, status, headers, config) {
                    alert("Error Receiving Data!")
                });
        }

        $scope.$watch(function () {
            if ($rootScope.itinerary == undefined || $rootScope.itinerary == null) {
                return null;
            }
            return $rootScope.itinerary;
        }, function () {
            if ($rootScope.CITY == null) {
                return;
            }
            if ($rootScope.itinerary == null) {
                return;
            }
            updateGlobalCity();
            updateAddAttrButtonPos();
        }, true);

        $rootScope.$watch(function () {
            return $rootScope.CITY;
        }, function (value) {
            if ($rootScope.CITYCHANGED == undefined || $rootScope.CITYCHANGED == null) {
                return;
            }
            if ($rootScope.CITYCHANGED == true) {
                $rootScope.CITYCHANGED = false;

                updateTypeAheadList();
                updateItinerary();
            }
        });

        $scope.addPlaceButtonClicked = function(btn){
            $scope.isInvalid = false;
            var modal = document.getElementById('myModal');
            modal.attributes['5'].value = false;
            modal.style.display = "block";
            modal.className = "modal fade in";
            $scope.currentButton = btn;
        };

        function hasPlace(placeName){
            for(var i = 0; i< $rootScope.places.length; i++){
                if(placeName == $rootScope.places[i].name){
                    return $rootScope.places[i];
                }
            }
            return false;
        }

        function addNewPlaceAt(btn, place) {
            var ADJUST_TOP = 10;
            var attr = getItineraryAttractionFromCityAttraction(place);
            attr.top = btn.top + ADJUST_TOP - 39;
            $rootScope.itinerary[btn.dayId].splice(btn.attrId,0, attr);

            updateAddAttrButtonPos();
        }

        $scope.hideAddPlaceModal = function(){
            var inputField = document.getElementById('addPlaceInputField');
            inputField.style.borderColor = "";
            inputField.style.boxShadow = "";
            inputField.style.outline = "";
            inputField.value = "";
            var modal = document.getElementById('myModal');
            hideModal(modal);
        }

        function showModal(modal){
            modal.attributes['5'].value = false;
            modal.style.display = "block";
            modal.className = "modal fade in";
        }

        function hideModal(modal){
            modal.attributes['5'].value = true;
            modal.style.display = "none";
            modal.className = "modal fade";
        }

        function showLoadingModal() {
            var loadingModal = document.getElementById("loadingModal");
           showModal(loadingModal);
        }

        function hideLoadingModal(){
            var loadingModal = document.getElementById("loadingModal");
            hideModal(loadingModal);
        }

        $scope.addPlaceToPlanner = function(){
            var inputField = document.getElementById('addPlaceInputField');

            var place = hasPlace(inputField.value);




            if( place != false ) {
                $scope.hideAddPlaceModal();
                showLoadingModal();
                $http.get('/attraction.JSON', {
                    params: {
                        id:place.id
                    }
                }).
                    success(function (data, status, headers, config) {
                        addNewPlaceAt($scope.currentButton, data);
                        hideLoadingModal();
                    }).
                    error(function (data, status, headers, config) {
                    });
            }
            else{
                inputField.style.borderColor = "rgba(239, 39, 0, 0.80)";
                inputField.style.boxShadow = "0 1px 1px rgba(0, 0, 0, 0.075) inset, 0 0 8px rgba(239, 39, 0, 0.60)";
                inputField.style.outline = "0 none";
            }


        }


    });

    app.controller('finalPageController', function ($scope, $http, $rootScope,$timeout,$location) {

        $scope.cityName = null;
        $scope.days = null;

        $scope.data = null;
        $scope.showPlanner = true;
        $scope.showMap = false;
        $scope.CITY = null;
        $scope.key = null
        $scope.showModal = false;

        $scope.categories = ["Outdoor Activities","Historical/Religious", "Landmarks","Education/Art","Natural","Leisure", "Others" ];
        $scope.categoryIsSelected = [true, true, true, true, true, true, true];
        $scope.showCategoryMenu = false;
        $scope.showApplyButton  = false;


        function loadFromUrl() {
            var url = $location.absUrl();
            url = decodeURIComponent(url);
            url = url.substring(url.indexOf('?') + 1);
            var params = url.split('&');
            var Params = {};
            for (param in params) {
                var x = params[param].split('=');
                x[1]= x[1].split('#')[0];
                Params[x[0]] = x[1];
            }
            $scope.key= Params.id;

        }

        loadFromUrl();

        function pad(n, width, z) {
            z = z || '0';
            n = n + '';
            return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
        }

        $scope.secondToHour = function(seconds){
            var hr = Math.floor((seconds/60.0)/60.0);
            var min = Math.floor(seconds/60.0)%60;
            var ret = pad(parseInt(hr),2)+ ":" + pad(parseInt(min),2);
            return ret;
        };

        $scope.setMap = function () {
            $rootScope.showLoading = true;
            $scope.showPlanner = false;
            $scope.showMap = true;
            $scope.isCollapsed = false;
            $timeout(function () {

                $scope.$broadcast("cityDataReceived", {City: $rootScope.CITY });
            }, 10, true);

        };


//        $scope.getCityDetails = function () {

            $scope.data = null;
            $scope.CITY = null;

            $http.get('/final.JSON', {
                params: {
                    id : $scope.key
//
                }
            }).
                success(function (data, status, headers, config) {
                    // TO DO. REMOVE THIS. AND GET THIS FROM THE JSON ITSELF //
//                    data.cityName = $scope.cityName;

                    $scope.data = data;
                    $scope.CITY = data;
                    $scope.setMap();
                }).
                error(function (data, status, headers, config) {
                    alert("Could Not Connect To Server");
                });
//        };

            $scope.copyUrlToClipBoard = function(){
                console.log($location.absUrl());
                window.prompt("Copy to clipboard: Ctrl+C, Enter", $location.absUrl());
            }

    });


})();