package models;

import play.db.jpa.Model;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import java.util.List;

/**
 * Created by sajit.m on 8/14/2014.
 */
@Entity
public class Itinerary extends Model {

    @ElementCollection
    public List<Destination> destinations;

//    public String key;
    public int numberOfDays;
    public String city;
//    public int time;

//    public void setTime(int time) {
//        this.time = time;
//    }

    public Itinerary(List<Destination> destinations, int numberOfDays,String city) {
        this.destinations = destinations;
        this.numberOfDays = numberOfDays;

        this.city = city;
    }
}
