package models;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by sajit.m on 9/8/2014.
 */
public class DestinationCluster {
    private Set<Destination> cluster;
    private double score;
    private long time;
    private Coordinates center;
    public static final long maxTime = 3000;
    public static long timeOffset = 32400;
    //public static final long timePerLocation = 7000;


    public DestinationCluster(){
        cluster = new HashSet<Destination>();
        center = new Coordinates();
    }

    public Set<Destination> getCluster() {
        return cluster;
    }

    public Coordinates getCenter() {
        return center;
    }

    public double getSocre() {
        return score;
    }

    public long getTime() {
        return time;
    }

    public void addTime(long time) {
        this.time+= time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public void addDestination(Destination destination){
        cluster.add(destination);
        this.score+=destination.getScore();
        //destination.setStartTime(time);
        time+=destination.getDuration();
        destination.setImageUrl();
    }

    public void removeDestination(Destination destination){
        cluster.remove(destination);
        time-=destination.getDuration();
    }

    public boolean isClusterFull(){
        return time > 25000;
    }

    public void modifyCenter(double latitude,double longitude){
        center.setLon(((cluster.size()-1)*center.getLon()+longitude)/cluster.size());
        center.setLat(((cluster.size() - 1) * center.getLat() + latitude) / cluster.size());
    }

    public long getTripDuration(){
        return time-timeOffset;
    }


}
