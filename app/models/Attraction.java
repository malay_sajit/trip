package models;

/**
 * Created by sajit.m on 9/12/2014.
 */
public class Attraction {
    private long id;
    private String name;
    private String category;

    public Attraction(String name, long id,String category) {
        this.name = name;
        this.id = id;
        this.category = category;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
