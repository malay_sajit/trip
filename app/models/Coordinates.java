package models;

/**
 * Created by sajit.m on 9/8/2014.
 */
public class Coordinates {
    private double lat;
    private double lon;

    public Coordinates() {
        this.lat = 0;
        this.lon = 0;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

}
