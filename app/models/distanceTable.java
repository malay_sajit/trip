package models;

import play.db.jpa.Model;

import javax.persistence.Entity;

/**
 * Created by sajit.m on 8/25/2014.
 */

@Entity
public class distanceTable extends Model {

    private String city;
    private long originId;
    private long destinationId;
    //int distance;
    private long time;

    public long getTime() {
        return time;
    }

    public distanceTable(String city, long originId, long destinationId, long time) {
        this.originId = originId;
        this.destinationId = destinationId;
        this.city = city;
        this.time = time;
    }

    public long getOriginId() {
        return originId;
    }

    public long getDestinationId() {
        return destinationId;
    }

    public String getCity() {
        return city;
    }
}

