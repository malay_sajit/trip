

/**
 * Created by sajit.m on 8/13/2014.
 */
package models;

import play.db.jpa.Model;
import play.vfs.VirtualFile;

import javax.persistence.Entity;
import javax.persistence.Transient;
import java.io.File;

@Entity
public class Destination extends Model {
    private String name;
    private String city;
    private int reviews;
    private double rating;
    private int myRank;
    private double score;
    private double latitiude;
    private double longitude;
    private long duration=7200;
    private String category = "Others";

    @Transient
    private long startTime;

    @Transient
    private String imageUrl;

    public int getReviews() {
        return reviews;
    }

    public double getRating() {
        return rating;
    }

    public String getCategory() {
        return category;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public long getDuration() {
        return duration;
    }

    public long getStartTime() {
        return startTime;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setImageUrl() {
        String imageUrl;
        imageUrl = "/public/images/attractions/"+this.city+"/"+this.name+".jpg";
        VirtualFile vf = VirtualFile.fromRelativePath(imageUrl);
        File f = vf.getRealFile();
        if(f.exists()){
            this.imageUrl=imageUrl;
        }
        else
            this.imageUrl="/public/images/attractions/default.jpg";
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public Destination(String name, String city, int reviews, double rating, double score) {
        this.name=name;
        this.city = city;
        this.reviews= reviews;
        this.rating = rating;
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public int getMyRank() {
        return myRank;
    }

    public String getCity() {
        return city;
    }

    public double getLatitiude() {
        return latitiude;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getScore() {
        return score;
    }

//    public static void getCityNames() {
//        Query query = JPA.em().createQuery("select DISTINCT(d.city) from Destination d");
//        List<String> cities = query.getResultList();
//    }
}