package models;

/**
 * Created by sajit.m on 9/11/2014.
 */
public class DestinationJson {
    private long id;
    private String name;
    private double latitiude;
    private double longitude;
    private long duration;
    private long startTime;
    private String imageUrl;
    private String category;

    public DestinationJson(long id, String name, double latitiude, double longitude, long duration, long startTime, String imageUrl,String category) {
        this.id = id;
        this.name = name;
        this.latitiude = latitiude;
        this.longitude = longitude;
        this.duration = duration;
        this.startTime = startTime;
        this.imageUrl = imageUrl;
        this.category = category;
    }
}
