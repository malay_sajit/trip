package models;

import play.db.jpa.Model;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.OneToOne;

/**
 * Created by sajit.m on 8/13/2014.
 */

@Entity
public class About extends Model {
    public String timing;
    public String timeToSpend;

    @Lob
    public String text;

    @OneToOne
    public Destination destination;

    public About(String text, String timing, String timeToSpend) {
        this.text = text;
        this.timing = timing;
        this.timeToSpend = timeToSpend;
    }
}
