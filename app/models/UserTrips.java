package models;

import play.db.jpa.Model;

import javax.persistence.Entity;
import javax.persistence.Lob;

/**
 * Created by sajit.m on 9/16/2014.
 */

@Entity
public class UserTrips extends Model {

    public String trip;

    @Lob
    public String destinaiton;

    public String getDestinaiton() {
        return destinaiton;
    }

    public String value;

    public UserTrips(String trip, String destinaiton) {
        this.trip = trip;
//        this.value = value;
        this.destinaiton = destinaiton;
    }
}
