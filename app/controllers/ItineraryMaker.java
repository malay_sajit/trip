package controllers;

import models.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Created by sajit.m on 9/8/2014.
 */
public class ItineraryMaker {
    private int days;
    private String city;
    private String category;
    private List<Destination> filteredLocations= new ArrayList<Destination>();
    private List<Destination> topLocations;
    private List<Destination> allLocations;
    private List<DestinationCluster> destinationList;
    private int isSelected[];
    private List<distanceTable> totalTime;
    private HashMap<Long,distanceTable> timeMap = new HashMap<Long, distanceTable>();
    private HashMap<String,Integer> categoryMap = new HashMap<String, Integer>();

    private int locationsPerDay[]={5,10,15,19,22};       // for the first 5 days
    private int mapForSmallTime[] = {20,15,12,10,8,8};
    private int mapForLargeTime[] = {60,45,35,28,22};

    public ItineraryMaker(int days, String city,String category) {
        this.days = days;
        this.city = city;
        this.category=category;
        initializeCategoryMap();
        filterList();
    }

    public int getNumberOfLocations(){
        return locationsPerDay[days-1];
    }


    public void getTimeSortedList(){
        //totalTime = new ArrayList<distanceTable>();
        totalTime = distanceTable.find("city = ?",city).fetch();
        Collections.sort(totalTime, new TimeSort());
        for(distanceTable d: totalTime){
            long key=d.getOriginId()*10000+d.getDestinationId();
            timeMap.put(key,d);
        }
    }

    public long getlargeTimeLimit(){
        int position = mapForLargeTime[days-1]*totalTime.size()/100;
        System.out.println("time limit"+totalTime.get(position).getTime());
        return totalTime.get(position).getTime();
    }

    public long getsmallTimeLimit(){
        //System.out.println("size: "+totalTime.size());
        int position = mapForSmallTime[days-1]*totalTime.size()/100;
        System.out.println("time limit"+totalTime.get(position).getTime());
        return totalTime.get(position).getTime();
    }

    public void initializeCategoryMap(){
        categoryMap.put("Outdoor Activities", 0);
        categoryMap.put("Historical/Religious", 1);
        categoryMap.put("Landmarks", 2);
        categoryMap.put("Education/Art", 3);
        categoryMap.put("Natural", 4);
        categoryMap.put("Leisure", 5);
        categoryMap.put("Others", 6);
    }

    public void filterList(){
//        System.out.println("in filterList");
        List<Destination> destinations =  Destination.find("city = ? order by score desc",city).fetch();
        for(Destination d: destinations){
            String[] categoryList = d.getCategory().split("\\|");
            for(String c:categoryList){
                System.out.println(c+" index : "+categoryMap.get(c));
                if(category.charAt(categoryMap.get(c))=='1') {
                    filteredLocations.add(d);
                    break;
                }
            }
        }
        Collections.sort(filteredLocations, new ScoreSort());
        Collections.reverse(filteredLocations);
    }

    public void getTopLocationsX(){
//        topLocations = Destination.find("city = ? order by score desc",city).fetch(getNumberOfLocations());
        topLocations = new ArrayList<Destination>(filteredLocations.subList(0, Math.min(getNumberOfLocations(),filteredLocations.size()-1)));
    }

    public void getAllLocationsX(){
//        allLocations =  Destination.find("city = ? order by score desc",city).fetch();
        allLocations = filteredLocations;
    }


    public void initializeCluster(){
        destinationList = new ArrayList<DestinationCluster>();
        isSelected = new int[allLocations.size()];
    }

    public void addAttraction(int i,DestinationCluster destinationCluster, Destination destination){
//
        isSelected[i]=1;
        destinationCluster.addDestination(destination);
        destinationCluster.modifyCenter(destination.getLatitiude(), destination.getLongitude());
    }

    public long getAverageTime(DestinationCluster destinationCluster,Destination destination){
        long time=0;
        for(Destination d: destinationCluster.getCluster()){
            long org= d.getId();
            time+=timeMap.get(org*10000+destination.getId()).getTime();
        }
        time/=destinationCluster.getCluster().size();
        return time;
    }

    public DestinationCluster createCluster(long timeLimit){
        DestinationCluster destinationCluster = new DestinationCluster();
        for (int i = 0; i < topLocations.size(); i++) {
            Destination destination = topLocations.get(i);
            if(isSelected[i]==0) {
                addAttraction(i,destinationCluster,destination);
                break;
            }
        }

        for (int i = 0; i< topLocations.size();i++){
            if(isSelected[i]!=0)
                continue;
            Destination destination = topLocations.get(i);
            long time = getAverageTime(destinationCluster,destination);
            if(time < timeLimit){
                destinationCluster.addTime(time);
                addAttraction(i,destinationCluster,destination);
            }
            if(destinationCluster.isClusterFull()==true)
                break;
        }
        //Just for debuuging
        System.out.println("set");
        for (Destination d : destinationCluster.getCluster()) {
            System.out.println(d.getName());
        }

        return destinationCluster;
    }

    public void createSmallCluster() {
        System.out.println("entered");
        initializeCluster();
        int sets = 0;
        long timeLimit=getsmallTimeLimit();
        while (sets < days) {
            destinationList.add(createCluster(timeLimit));
            sets++;
        }
    }

    public boolean allCLusterFull(){
        for(int i=0;i<destinationList.size();i++){
            if(destinationList.get(i).isClusterFull()==false)
                return false;
        }
        return true;
    }
    public void growCluster() {
        System.out.println("enterned again");
        long timeLimit = getlargeTimeLimit();
        int count = 0;
        while(allCLusterFull()==false) {
            for (int j = 0; j < destinationList.size(); j++) {
                if (destinationList.get(j).isClusterFull() == true)
                    continue;
                DestinationCluster destinationCluster = destinationList.get(j);
                for (int i = 0; i < allLocations.size(); i++) {
                    if (isSelected[i] != 0) {
                        continue;
                    }
                    Destination destination = allLocations.get(i);
                    long time = getAverageTime(destinationCluster, destination);
                    if (time < timeLimit) {
                        destinationCluster.addTime(time);
                        addAttraction(i, destinationCluster, destination);
                    }
                    if (destinationCluster.isClusterFull() == true)
                        break;

                }
            }
            count++;
            timeLimit*=1.2;
            if(count>8)
                break;
        }

        System.out.println("set");
            for (DestinationCluster d : destinationList) {
                for (Destination d1 : d.getCluster()) {
                System.out.println(d1.getName()+d1.getImageUrl()+d1.getStartTime());
            }
                System.out.println("total time:"+d.getTime());
                System.out.println("-------------------------");
            }
    }

    public List<DestiantionsPerDay> getResult(){
        System.out.println("In get result");
        List<DestiantionsPerDay> destiantionsPerDayList = new ArrayList<DestiantionsPerDay>();
        for(int i=0;i<destinationList.size();i++){
            DestinationCluster destinationCluster = destinationList.get(i);
            List<Destination> destinations = convertClusterToList(destinationCluster);
            Matrix dist=getDistanceMatrixPerCluster(destinations);

            int []order = new int[destinations.size()];
            for(int j = 0 ;j<destinations.size(); j++)
                order[j] = j;

            order = Utility.getBestPermutation(order, dist);

            destinationCluster.setTime(0);
            List<Destination> orderedDestination = new ArrayList<Destination>();

            Destination origin = destinations.get(order[0]);
            origin.setStartTime(DestinationCluster.timeOffset);
            orderedDestination.add(origin);
            destinationCluster.addTime(origin.getDuration()+origin.getStartTime());
            int lunchFlag=0;
            for(int j=1; j<order.length ;j++){
                Destination destination=destinations.get(order[j]);
                long time=(long) ((timeMap.get(origin.getId()*10000+destination.getId()).getTime())*1.5);
                if(destinationCluster.getTime()>42000 && lunchFlag==0){
                    destinationCluster.addTime(4500);
                    lunchFlag = 1;
                }

                destination.setStartTime(destinationCluster.getTime()+time);
                orderedDestination.add(destinations.get(order[j]));

                destinationCluster.addTime(time+destination.getDuration());
                origin = destination;

                System.out.println(destination.getName()+destination.getStartTime());
            }

            List<DestinationJson> destinationJsons = new ArrayList<DestinationJson>();
            for(Destination d: orderedDestination){
                destinationJsons.add(new DestinationJson(d.getId(),d.getName(),d.getLatitiude(),d.getLongitude(),d.getDuration(),d.getStartTime(),d.getImageUrl(),d.getCategory()));
            }

            destiantionsPerDayList.add(new DestiantionsPerDay(destinationJsons,"Day "+(i+1)));
        }
        return destiantionsPerDayList;
    }

    public List<Destination> convertClusterToList(DestinationCluster destinationCluster){
        List<Destination> destinations = new ArrayList<Destination>();
        for(Destination d: destinationCluster.getCluster()){
            destinations.add(d);
        }
        return destinations;
    }

    public Matrix getDistanceMatrixPerCluster(List<Destination> destinations){
        int size = destinations.size();
        Matrix dist = new Matrix(size);
        for(int i = 0;i<size;i++)
            dist.add(new MatrixRow(size));
        for(int i=0;i<destinations.size();i++){
            Destination org = destinations.get(i);
            for(int j=0;j<destinations.size();j++){
                Destination des = destinations.get(j);
                dist.get(i).add(timeMap.get(org.getId()*10000+des.getId()).getTime());

            }
        }
        return dist;
    }

    public double getGratificationScore(){
        double score=0;
        double time=0;
        for(DestinationCluster d : destinationList){
            score+=d.getSocre();
            time+=d.getTime();
        }
        return score/(time/3600);
    }

    public List<DestiantionsPerDay> createItinerary(){
        getTimeSortedList();
        getTopLocationsX();
        getAllLocationsX();
        createSmallCluster();
        growCluster();
        return getResult();
    }


}
