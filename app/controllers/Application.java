package controllers;

import com.google.gson.JsonElement;
import models.*;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import play.libs.WS;
import play.mvc.Controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//import org.json.JSONException;

//import javax.json.JsonObject;

//import models.*;

public class Application extends Controller {

    public static void index() {
        render();
    }

    public static void enterCity(String city, int days){
        render(city,days);
    }



    public static void dragndrop(){
        render();
    }

    public static void showCity(String city, int days ) {
    }

    public static void getDistance(){

        WS.HttpResponse res = WS.url("https://maps.googleapis.com/maps/api/distancematrix/json?origins=18.5203, 73.8567&destinations=18.9750, 72.8258&mode=driving&key=AIzaSyDsqTnA6BE0n3mrKOZGrwPIRHhu2yhXhz4").get();
        JsonElement json = res.getJson();
        renderJSON(json);
    }

    public static void getCity() {
        List<Destination> destinations = Destination.all().fetch();
        renderJSON(destinations);
    }

    public static void getAllDistance(String city, int days,String category){
        System.out.println("New TEST");
        ItineraryMaker itineraryMaker = new ItineraryMaker(days,city,category);
        renderJSON(itineraryMaker.createItinerary());
    }

    public static void getDistanceMatrix(String city){
        List<Destination> len = Destination.find("city = ? ", city).fetch();
        long curr_id=0;
        long last_id=len.get(len.size()-1).getId();
        while (curr_id < last_id) {
            List<Destination> list = Destination.find("city = ? and id > ? ", city, curr_id).fetch(1);
            Destination origin = list.get(0);
            curr_id=origin.getId();
            List<Destination> destinations = Destination.find("city = ? ", city).fetch();
            System.out.println("current_id="+curr_id);
            int index = 0;
            while (index < destinations.size()) {
                int start = index;
                String org = origin.getLatitiude() + "," + origin.getLongitude();
                String dest = "";
                for (int i = 0; index < destinations.size() && i < 25; i++, index++) {
                    dest += destinations.get(index).getLatitiude() + "," + destinations.get(i).getLongitude() + "|";
                }
                if (dest != "") {
                    dest = dest.substring(0, dest.length() - 1);
                    //WS.HttpResponse res = WS.url("https://maps.googleapis.com/maps/api/distancematrix/json?origins=" + org + "&destinations=" + dest + "&mode=driving&key=AIzaSyCHyVgzmY7NVGKfXx3bXV_H-dD86gsa0Aw").get();
                    WS.HttpResponse res = WS.url("https://maps.googleapis.com/maps/api/distancematrix/json?origins=" + org + "&destinations=" + dest + "&mode=driving&key=AIzaSyDsqTnA6BE0n3mrKOZGrwPIRHhu2yhXhz4").get();
                    //WS.HttpResponse res = WS.url("https://maps.googleapis.com/maps/api/distancematrix/json?origins=" + org + "&destinations=" + dest + "&mode=driving&key=AIzaSyCbTFMbj2hsO0qQGvl3yPxvwjKMtNTPW4Y").get();
                   // System.out.println(res.getString());

                    Matrix total = Utility.get2DMatrixFromRespnse(res.getString());
                    MatrixRow time = (MatrixRow) total.get(0);
                   // System.out.println(time.size());
                    for (int i = 0; i < time.size(); i++) {
                        long t = time.get(i);
                        new distanceTable(city, origin.getId(), destinations.get(start + i).getId(), t).save();

                    }

                }
            }
        }
    }

    public static void getResult() throws IOException {
        System.out.println("New TEST");
//        String imageUrl = "/public/images/back.png";
//        VirtualFile vf = VirtualFile.fromRelativePath(imageUrl);
//        File f = vf.getRealFile();
//        System.out.println(f);
//        //File f = new File(imageUrl);
//        if(f.exists())
//            System.out.println("exists");
//        else
//
//            System.out.println("does not exist");
//        Long t = Long.valueOf(1);
//        List<Destination> l = Destination.find("id = ?",t).fetch();
//        List<Itinerary> list= Itinerary.find("dest = ? ", "111").fetch();
//        Itinerary i = list.get(0);
//        i.setTime(100);
//        i.save();

//
//        CategoryMaker categoryMaker = new CategoryMaker();
//        HashMap<String,List<String>> categoryMap;
//
//
//        try {
//            categoryMaker.createMap();
//            categoryMaker.getCategoryForDestination();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        HashMap<String,Integer> editCategory= new HashMap<String, Integer>();
        editCategory.put("Outdoor Activities", 0);
        editCategory.put("Historical/Religious", 0);
        editCategory.put("Landmarks", 0);
        editCategory.put("Education/Art", 0);
        editCategory.put("Natural", 0);
        editCategory.put("Leisure", 0);
        editCategory.put("Others", 0);

        List<Destination> destinations = Destination.all().fetch();
        for(Destination d: destinations){
            String newCategory="";
            List<String> categoryList = new ArrayList<String>();
            String[] categories= d.getCategory().split("\\|");
            for(String s: categories){
                if(editCategory.get(s)==0){
                    categoryList.add(s);
                    editCategory.put(s,1);
                }
            }
            for(String c: categoryList){
                newCategory+=c+"|";
            }
            newCategory = newCategory.substring(0, newCategory.length() - 1);
//            d.setCategory(newCategory);
            System.out.println(newCategory);
            d.setCategory(newCategory);
            d.save();
            for(String key: editCategory.keySet()){
                editCategory.put(key,0);
            }
        }

    }



    public static void attractionList(String city){
        List<Destination> list = Destination.find("city = ?  ", city).fetch();

        List<Attraction> attractionList = new ArrayList<Attraction>();

        for(Destination d: list){
            Attraction attraction = new Attraction(d.getName(),d.getId(),d.getCategory());
            attractionList.add(attraction);
//            System.out.println(attraction);

        }
        renderJSON(attractionList);

    }

    public static void getAttractionOnId(long id){
        List<Destination> list = Destination.find("id = ?  ", id).fetch();
        Destination d = list.get(0);
        d.setImageUrl();
        DestinationJson dj = new DestinationJson(d.getId(),d.getName(),d.getLatitiude(),d.getLongitude(),d.getDuration(),d.getStartTime(),d.getImageUrl(),d.getCategory());
        renderJSON(dj);

    }

    public static void finalPage(){
//        List<Destination> destinations = Destination.find("city = ?", "rome").fetch();
////        System.out.println(destinations.size());
//        Itinerary itinerary = new Itinerary(destinations, 3 , "rome");
//        itinerary.save();
//        List<Itinerary> itineraries = Itinerary.find("id = ? ", (long) 5).fetch();
//        System.out.println(itineraries.get(0).destinations.get(0).getCity());

//        Destination destination = destinations.get(0);
       render();
    }

    public static void fianlTrip(String id){
//        ItineraryMaker itineraryMaker = new ItineraryMaker(3,"paris","1111111");
//        renderJSON(itineraryMaker.createItinerary());
        List<UserTrips> userTripsList= UserTrips.find("trip = ?", id).fetch();
        renderJSON(userTripsList.get(0).getDestinaiton());


    }


    public static void fianlTriphash(String params) {
//        System.out.println("INefjfjksdfh");
        JSONParser parser = new JSONParser();
//        System.out.println(request.params);
        JSONArray jsonArray = null;
        try {
            Object obj = parser.parse(request.params.get("body"));
            jsonArray = (JSONArray) obj;
//            System.out.println((jsonArray).toJSONString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String hash = md5Java((jsonArray).toJSONString());
        System.out.println("outside save");
//        if(UserTrips.find("trip = ?", hash).fetch()==null) {
//            System.out.println("inside save");
            new UserTrips(hash, (jsonArray).toJSONString()).save();

        renderJSON(hash);
    }

    public static String md5Java(String message){
        String digest = null;
        try { MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] hash = md.digest(message.getBytes("UTF-8"));
            //converting byte array to Hexadecimal String
             StringBuilder sb = new StringBuilder(2*hash.length);
             for(byte b : hash){ sb.append(String.format("%02x", b&0xff));
             } digest = sb.toString();
        } catch (UnsupportedEncodingException ex)
        { //Logger.getLogger(StringReplace.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex)
        { //Logger.getLogger(StringReplace.class.getName()).log(Level.SEVERE, null, ex);
        }
        return digest;
    }

       }