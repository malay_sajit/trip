package controllers;

import models.Destination;

import java.util.Comparator;

/**
 * Created by sajit.m on 9/15/2014.
 */
public class ScoreSort implements Comparator<Destination> {

    @Override
    public int compare(Destination o1, Destination o2) {
        if(o1.getScore() > o2.getScore())
            return 1;
        else if(o1.getScore() == o2.getScore())
            return 0;
        else return -1;
    }
}
