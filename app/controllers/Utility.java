package controllers;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * Created by malayk on 8/22/14.
 */


public final class Utility {
    public final static long INF = 1000000000;
    static JSONObject getJSONFromString(String jsonString){
        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = null;
        try {
            jsonObject = (JSONObject)jsonParser.parse(jsonString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public static Matrix get2DMatrixFromRespnse(String json){
        System.out.println(json);
        JSONObject jsonResponse = Utility.getJSONFromString(json);
        JSONArray jsonArray = (JSONArray) jsonResponse.get("rows");
        int numOfRows = jsonArray.size();
        int numberOfCols = ((JSONArray)((JSONObject)jsonArray.get(0)).get("elements")).size();
        Matrix dist = new Matrix(numOfRows);
        for(int i = 0;i<numOfRows;i++)
            dist.add(new MatrixRow(numberOfCols));

        for(int i = 0;i<numOfRows;i++) {
            JSONArray subArray = (JSONArray)((JSONObject)jsonArray.get(i)).get("elements");
            for (int j  = 0;j<subArray.size(); j++) {
                JSONObject obj = (JSONObject) subArray.get(j);
                if(((String)obj.get("status")).equals("OK")) {
                    obj = (JSONObject)obj.get("duration");
                    dist.get(i).add((Long) obj.get("value"));
                }
                else {
                    dist.get(i).add(INF);
                }
            }
        }
        return dist;
    }

    public static boolean next_permutation(int[] p) {
        for (int a = p.length - 2; a >= 0; --a)
            if (p[a] < p[a + 1])
                for (int b = p.length - 1;; --b)
                    if (p[b] > p[a]) {
                        int t = p[a];
                        p[a] = p[b];
                        p[b] = t;
                        for (++a, b = p.length - 1; a < b; ++a, --b) {
                            t = p[a];
                            p[a] = p[b];
                            p[b] = t;
                        }
                        return true;
                    }
        return false;
    }

    private static Long totalWeightForPermutation(final int[] array, final Matrix weight){
        Long totalWeight = (long) 0;
        for(int i = 1;i<array.length; i++)
            totalWeight += weight.get(array[i-1]).get(array[i]);
        return totalWeight;
    }

    public static int[] getBestPermutation(int[] array,final Matrix weight){
        int []ret = new int[array.length];
        System.arraycopy(array, 0, ret, 0, array.length);
        Long bestWeight = totalWeightForPermutation(array, weight);
        do {
            Long newWeight = totalWeightForPermutation(array, weight);
            if(newWeight < bestWeight){
                bestWeight = newWeight;
                System.arraycopy(array, 0, ret, 0, array.length);
            }
        }while (next_permutation(array));
        return ret;
    }

}
