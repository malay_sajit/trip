package controllers;

import models.Destination;
import play.vfs.VirtualFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by sajit.m on 9/12/2014.
 */
public class CategoryMaker {
    private HashMap<String,List<String>> categoryMap = new HashMap<String, List<String>>();
    HashMap<String,Long> categoryToTime = new HashMap<String, Long>();

    public CategoryMaker(){
        createCategoryTimeMap();
    }

    public HashMap<String, List<String>> getCategoryMap() {
        return categoryMap;
    }

    public HashMap<String,List<String>> createMap() throws IOException {
        String file = "/tag_category.csv";
        VirtualFile vf = VirtualFile.fromRelativePath(file);
        File f = vf.getRealFile();
        BufferedReader br = new BufferedReader(new FileReader(f));
        try {
            String line = br.readLine();

            while (line != null) {
                createMapFromFile(line);
                line = br.readLine();
            }
        } finally {
            br.close();
        }
        return categoryMap;
    }

    public void createMapFromFile(String line){
        String[] keyValue = line.split("\\|");
        addToCategoryMap(keyValue[1],keyValue[0]);
    }


    public void addToCategoryMap(String key, String value){
        List<String> category = categoryMap.get(key);
        if(category!=null){
            category.add(value);
        }
        else{
            List<String> categoryList = new ArrayList<String>();
            categoryList.add(value);
            categoryMap.put(key,categoryList);
        }

    }


    public void getCategoryForDestination() throws IOException {
        String file = "/city.csv";
        VirtualFile vf = VirtualFile.fromRelativePath(file);
        File f = vf.getRealFile();
        BufferedReader br = new BufferedReader(new FileReader(f));
        try {
            String line = br.readLine();

            while (line != null) {
                getNameandCategory(line);
//                Destination destination = getDestinationFromCsv(line);
                line = br.readLine();
            }
        } finally {
            br.close();
        }
    }

    public void getNameandCategory(String line){
        String[] list = line.split(",");
        String name = list[4];
        double longitude = Double.parseDouble(list[7]);
        String category = list[list.length-1];
//        System.out.println(category);
        addCategoryToDestination(name,category,longitude);

    }

    public void addCategoryToDestination(String name, String category,double longitude){
//        Destination destination = (Destination) Destination.find("name = ? ", name).fetch();
        String s="";
        String[] categoryList = category.split("\\|");
        for(String key :categoryList){
            List<String> list = categoryMap.get(key);
            if(list==null)
                s+="Others|";
            else {
                for (String value : list) {
                    s += value + "|";
                }
            }
        }
        s = s.substring(0, s.length() - 1);
        addDurationAndCategory(name, s,longitude);
        System.out.println(name+" category: "+s);
    }
    public void createCategoryTimeMap(){
        categoryToTime.put("Outdoor Activities", (long) 9000);
        categoryToTime.put("Historical/Religious", (long) 4500);
        categoryToTime.put("Landmarks", (long) 5400);
        categoryToTime.put("Education/Art", (long) 7200);
        categoryToTime.put("Natural", (long) 6300);
        categoryToTime.put("Leisure", (long) 7200);
        categoryToTime.put("Others", (long) 3600);
    }

    public void addDurationAndCategory(String name, String s,double longitude){
        List<Destination> destinations =  Destination.find("name = ? and longitude = ? ", name,longitude).fetch();
        long time = 0;
        if(destinations == null)
            return;
        else {
            String[] category = s.split("\\|");
            for (String c : category) {
                time += categoryToTime.get(c);
            }
            time /= category.length;
        }
        if(destinations.size()>0) {
            Destination destination = destinations.get(0);
            destination.setCategory(s);
            destination.setDuration(time);
            destination.save();
        }
    }

}
