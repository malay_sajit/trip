package controllers;

import models.distanceTable;

import java.util.Comparator;

/**
 * Created by sajit.m on 9/9/2014.
 */
public class TimeSort implements Comparator<distanceTable> {

    @Override
    public int compare(distanceTable o1, distanceTable o2) {
        if(o1.getTime()> o2.getTime())
            return 1;
        else if(o1.getTime() == o2.getTime())
            return 0;
        else return -1;
    }
}

